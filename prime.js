let prime = []

for (let i = 1; i <= 100; i++) {
    // 1
    let numbers = []
    for (let j = 1; j <= i; j++) {
        if (i % j === 0) {
            if (j === 1) {
                numbers.push(j)
            } else if (i != j) {
                numbers.push(null)
            } else {
                numbers.push(j)
            }
        }
    }
    if (numbers.length === 2) {
        prime.push([...numbers][1])
    }
}

console.log(...prime)