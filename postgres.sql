create table ProjectStaffEmail(
Id serial primary key,
ProjectCode varchar(30) not null,
StaffEmail varchar(255) not null );

create table Project (
Id serial primary key,
ProjectCode varchar(30) not null unique,
ProjectName varchar(255) not null unique,
ProjectStatus varchar(30) not null,
LastedDate date default now() );

create table ProjectModule (
ModuleCode serial primary key,
ProjectCode varchar(30) not null,
ModuleName varchar(255) not null );

create table IssueLog(
Id serial primary key,
IssueId varchar(10) not null unique,
IssueDate date default now(),
IssueSummary varchar(255),
IssueDescription varchar(255),
ProjectCode varchar(30) not null,
ModuleCode int not null,
DefectType varchar(30) not null,
IssueStatus varchar(30) not null,
IssueSeverity varchar(30) not null,
IssuePriority varchar(30) not null,
TargetFixDate date,
ClosedDate date,
IssueComment varchar(255) );

create table MasterInfo(
Id serial primary key,
MasterCode int not null unique,
MasterName varchar(30) not null,
MasterTypeCode int not null unique);

create table MasterType(
Id serial primary key,
MasterTypeCode int not null unique,
MasterTypeName varchar(30) not null );

create table IssueLogFile(
AttachFileId serial primary key,
IssueId varchar(30) not null,
AttachFileName varchar(255),
AttachFileDate date default now() );

alter table ProjectStaffEmail
add constraint fk_Project_ProjectStaffEmail
    foreign key (ProjectCode)
    references Project(ProjectCode);

alter table ProjectModule
add constraint fk_Project_ProjectModule
    foreign key (ProjectCode)
    references Project(ProjectCode);

alter table IssueLog
add constraint fk_Project_IssueLog
    foreign key (ProjectCode)
    references Project(ProjectCode);

alter table IssueLog
add constraint fk_ProjectModule_IssueLog
    foreign key (ModuleCode)
    references ProjectModule(ModuleCode);

-- alter table IssueLog
-- add constraint fk_MasterInfo_IssueLog
--     foreign key (DefectType)
--     references MasterInfo(MasterCode);

-- alter table MasterInfo
-- add constraint fk_MasterType_MasterInfo
--     foreign key (MasterTypeCode)
--     references MasterType(MasterTypeCode);

alter table IssueLogFile
add constraint fk_IssueLog_IssueLogFile
    foreign key (IssueId)
    references IssueLog(IssueId);