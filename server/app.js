const koa = require('koa')
const path = require('path')
const cors = require('@koa/cors')
const static = require('koa-static')
const bodyparser = require('koa-bodyparser')
const Routes = require('./controller/routes')
const Pool = require('./lib/db').getConnection
const PORT = process.env.PORT || 3000

const app = new koa()
    .use(static(path.resolve(__dirname, '../client/build')))
    .use(Pool)
    .use(cors())
    .use(bodyparser())
    .use(Routes.router)
    .listen(PORT)


//! const RedisSession = require('./lib/sess')
//? --- Redis Session ---
/*
 *app.keys = [`redis session`]
 *RedisSession(app)
 *app.use(async (ctx, next) => {
 *    let count = ctx.session.views || 0
 *    ctx.session.views = ++count
 *    ctx.body = `${count} views`
 *})
*/