let Issues = require('../repository/issue.js')
module.exports = {
    list: async ctx => {
        let db = await ctx.pool.connect()
        try {
            let result = await Issues.findIssue(db)
            ctx.body = result
        } catch (err) {
            console.error(`${err}`)
            ctx.body = false
        }
    },
    create: async ctx => {
        let db = await ctx.pool.connect()
        try {
            let insertId = await Issues.createIssue(db, ctx.request.body)
            ctx.body = insertId
        } catch (err) {
            ctx.body = "Data is not correct, Please try again"
        } finally {
            db.release()
        }
    },
    edit: async ctx => {
        let db = await ctx.pool.connect()
        try {
            await db.query('BEGIN')
            await Issues.editIssue(db, ctx.request.body)
            await Issues.editIssueProjectDate(db, ctx.request.body)
            await db.query('COMMIT')
            ctx.body = true
        } catch (err) {
            await db.query('ROLLBACK')
            ctx.body = false
        } finally {
            db.release()
        }
    },
    delete: async ctx => {
        let db = await ctx.pool.connect()
        try {
            let result = await Issues.deleteIssue(db, ctx.request.body.id)
            ctx.body = true
        } catch (err) {
            ctx.body = false
        } finally {
            db.release()
        }
    }
}