const fs = require('fs')
const path = require('path')
const Router = require('koa-router')
const Project = require('./project')
const Issue = require('./issue')
const router = new Router()
    //! Create Project Route
    .get('/project', Project.list)
    .post('/project', Project.create)
    .patch('/project/edit', Project.edit)
    .delete('/project/delete', Project.delete)
    //! Issue Route
    .get('/issue', Issue.list)
    .post('/issue',Issue.create)
    .patch('/issue/edit', Issue.edit)
    .delete('/issue/delete', Issue.delete)

    //static file
    .get('*', async ctx => {
        ctx.body = fs.readFileSync(path.resolve(path.join('build', 'index.html')), 'utf-8')
    })
    
module.exports = {
    router: router.routes()
}
