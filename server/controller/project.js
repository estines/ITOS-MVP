const Projects = require('../repository/project')

module.exports = {
    list: async ctx => {
        let db = await ctx.pool.connect()
        try {
            let projectList = await Projects.findProjects(db)
            let staffEmailList = await Projects.findProjectStaffEmails(db)
            let moduleList = await Projects.findProjectModule(db)
            let dataList = projectList.map(p => {
                let staffEmails = staffEmailList.map(s => {
                    return {
                        Id: s.id,
                        ProjectCode: s.projectcode,
                        StaffEmail: s.staffemail
                    }
                })
                staffEmails = staffEmails.filter(s => s.ProjectCode === p.projectcode)
                let modules = moduleList.map(m => {
                    return {
                        ModuleCode: m.modulecode,
                        ProjectCode: m.projectcode,
                        ModuleName: m.modulename
                    }
                })
                modules = modules.filter(m => m.ProjectCode === p.projectcode)
                return {
                    Id: p.id,
                    ProjectCode: p.projectcode,
                    ProjectName: p.projectname,
                    ProjectStaffEmail: staffEmails,
                    ProjectModule: modules,
                    ProjectStatus: p.projectstatus,
                    LastedUpdate: p.date
                }
            })
            ctx.body = dataList
        } catch (err) {
            ctx.body = false
        } finally {
            db.release()
        }
    },
    create: async ctx => {
        let db = await ctx.pool.connect()
        try {
            await db.query(`BEGIN`)
            let projectId = await Projects.createProject(db, ctx.request.body)
            let emailId = await Promise.all(ctx.request.body.ProjectStaffEmail.map(async item => {
                let id = await Projects.createProjectStaffEmail(db, { ProjectCode: ctx.request.body.ProjectCode, StaffEmail: item.StaffEmail })
                return id
            }))
            let moduleId = await Promise.all(ctx.request.body.ProjectModule.map(async item => {
                let id = await Projects.createProjectModule(db, { ProjectCode: ctx.request.body.ProjectCode, ModuleName: item.ModuleName })
                return id
            }))
            await db.query(`COMMIT`)
            ctx.body = { Id: projectId, StaffEmails: emailId, Modules: moduleId }
        } catch (err) {
            await db.query(`ROLLBACK`)
            console.log(`${err}`)
            ctx.body = false
        } finally {
            db.release()
        }
    },
    edit: async ctx => {
        let db = await ctx.pool.connect()
        try {
            let prevStaffEmails = await Projects.findProjectStaffEmailByCode(db, ctx.request.body.ProjectCode)
            let prevModules = await Projects.findProjectModuleByCode(db, ctx.request.body.ProjectCode)
            let createStaff = [], updateStaff = [], deleteStaff = [], createModule = [], updateModule = [], deleteModule = []
            let emailId, moduleId
            prevStaffEmails.map((prev, index) => {
                let checker = ctx.request.body.ProjectStaffEmail.map((curr, idx) => {
                    if (curr.Id === prev.id) {
                        updateStaff.push(curr)
                        return true
                    }
                    return false
                })
                !checker[index] ? deleteStaff.push(prev) : false
            })
            ctx.request.body.ProjectStaffEmail.map((item, index) => {
                let checker = updateStaff.map(u => {
                    if (u.Id === item.Id) {
                        return true
                    }
                    return false
                })
                !checker[index] ? createStaff.push(item) : false
            })
            prevModules.map((prev, index) => {
                let checker = ctx.request.body.ProjectModule.map((curr, idx) => {
                    if (curr.ModuleCode === prev.modulecode) {
                        updateModule.push(curr)
                        return true
                    }
                    return false
                })
                !checker[index] ? deleteModule.push(prev) : false

            })
            ctx.request.body.ProjectModule.map((item, index) => {
                let checker = updateModule.map(u => {
                    if (u.ModuleCode === item.ModuleCode) {
                        return true
                    }
                    return false
                })
                !checker[index] ? createModule.push(item) : false
            })
            await db.query('BEGIN')
            await Projects.editProject(db, ctx.request.body)
            if (createStaff.length !== 0) {
                emailId = await Promise.all(createStaff.map(async item => {
                    let id = await Projects.createProjectStaffEmail(db, item)
                    return id
                }))
            }
            if (createModule.length !== 0) {
                moduleId = await Promise.all(createModule.map(async item => {
                    let id = await Projects.createProjectModule(db, item)
                    return id
                }))
            }
            if (updateStaff.length !== 0) {
                await Promise.all(updateStaff.map(async item => {
                    await Projects.editProjectStaffEmail(db, item)
                }))
            }
            if (updateModule.length !== 0) {
                await Promise.all(updateModule.map(async item => {
                    await Projects.editProjectModule(db, item)
                }))
            }
            if (deleteStaff.length !== 0) {
                await Promise.all(deleteStaff.map(async item => {
                    await Projects.removeProjectStaffEmail(db, item.id)
                }))
            }
            if (deleteModule.length !== 0) {
                await Promise.all(deleteModule.map(async item => {
                    await Projects.removeProjectModule(db, item.modulecode)
                }))
            }
            await db.query('COMMIT')
            let newStaffEmail = createStaff.map((item, index) => {
                item.Id = emailId[index]
                return item
            })
            let newModule = createModule.map((item, index) => {
                item.ModuleCode = moduleId[index]
                return item
            })
            ctx.body = { ProjectStaffEmail: [...updateStaff, ...newStaffEmail], ProjectModule: [...updateModule, ...newModule] }
        } catch (err) {
            console.log(`${err}`)
            await db.query('ROLLBACK')
            ctx.body = false
        } finally {
            db.release()
        }
    },
    delete: async ctx => {
        let { ProjectCode, Id } = ctx.request.body
        let db = await ctx.pool.connect()
        try {
            await db.query('BEGIN')
            await Projects.removeProjectModuleByProjectCode(db, ProjectCode)
            await Projects.removeProjectStaffEmailByProjectCode(db, ProjectCode)
            await Projects.removeProject(db, ProjectCode)
            await db.query('COMMIT')
            ctx.body = { message: 'success!!'}
        } catch (err) {
            await db.query('ROLLBACK')
            ctx.body = false
        } finally {
            db.release()
        }
    }
}