module.exports = {
    createIssue,
    findIssue,
    editIssue,
    editIssueProjectDate,
    deleteIssue
}

async function createEntity(row) {
    return {
        Id: row.id,
        IssueID: row.issueid,
        IssueDate: row.issuedates,
        Summary: row.issuesummary,
        Description: row.issuedescription,
        Project: row.projectcode,
        Module: row.modulecode,
        DefectType: row.defecttype,
        Status: row.issuestatus,
        Severity: row.issueseverity,
        Priority: row.issuepriority,
        TargetFixDate: row.tfd,
        ClosedDate: row.csd,
        Comment: row.issuecomment
    }
}

async function findIssue(db) {
    let sql = `select *,to_char("issuedate",'DD/MM/YYYY') as IssueDates,
    to_char("targetfixdate",'DD/MM/YYYY') as TFD,to_char("closeddate",'DD/MM/YYYY') as CSD from issuelog`
    let { rows } = await db.query(sql)
    let newRows = await Promise.all(rows.map(async item => { return await createEntity(item) }))
    return newRows
}

async function createIssue(db, row) {
    let sql = [
        `insert into issuelog
        (issueid, issuedate,issuesummary , 
        issuedescription, projectcode, modulecode,
        defecttype, issuestatus, issueseverity, 
        issuepriority, targetfixdate, closeddate, issuecomment) values
         ('${row.IssueID}','now()','${row.Summary}',
         '${row.Description}','${row.Project}','${row.Module}',
         '${row.DefectType}','${row.Status}','${row.Severity}',
         '${row.Priority}','${row.TargetFixDate}','${row.ClosedDate}', '${row.Comment}')
         returning id`,
        `insert into issuelog (issueid, issuedate,issuesummary , 
        issuedescription, projectcode, modulecode,
        defecttype, issuestatus, issueseverity, 
        issuepriority, closeddate, issuecomment) values
         ('${row.IssueID}','now()','${row.Summary}',
         '${row.Description}','${row.Project}','${row.Module}',
         '${row.DefectType}','${row.Status}','${row.Severity}',
         '${row.Priority}','${row.ClosedDate}', '${row.Comment}')
         returning id`,
        `insert into issuelog
        (issueid, issuedate,issuesummary , 
        issuedescription, projectcode, modulecode,
        defecttype, issuestatus, issueseverity, 
        issuepriority, targetfixdate, issuecomment) values
         ('${row.IssueID}','now()','${row.Summary}',
         '${row.Description}','${row.Project}','${row.Module}',
         '${row.DefectType}','${row.Status}','${row.Severity}',
         '${row.Priority}','${row.TargetFixDate}', '${row.Comment}')
         returning id`,
        `insert into issuelog
        (issueid, issuedate,issuesummary , 
        issuedescription, projectcode, modulecode,
        defecttype, issuestatus, issueseverity, 
        issuepriority, issuecomment) values
         ('${row.IssueID}','now()','${row.Summary}',
         '${row.Description}','${row.Project}','${row.Module}',
         '${row.DefectType}','${row.Status}','${row.Severity}',
         '${row.Priority}', '${row.Comment}')
         returning id`]

    if (row.TargetFixDate !== "" && row.ClosedDate !== "") {
        sql = sql[0]
    } else if (row.TargetFixDate !== "" && row.ClosedDate === "") {
        sql = sql[2]
    } else if (row.TargetFixDate === "" && row.ClosedDate !== "") {
        sql = sql[1]
    } else {
        sql = sql[3]
    }
    try {
        let { rows } = await db.query(sql)
        return rows[0].id
    } catch (err) {
        console.error(`${err}`)
    }
}

async function editIssue(db, row) {
    let sql = [
        // all
        `update IssueLog set issuedate = 'now()' , issuesummary = '${row.Summary}', issuedescription =
    '${row.Description}', projectcode = '${row.Project}', modulecode = '${row.Module}', defecttype = 
    '${row.DefectType}', issuestatus = '${row.Status}', issueseverity = '${row.Severity}',
    issuepriority = '${row.Priority}', targetfixdate = '${row.TargetFixDate}',closeddate = '${row.ClosedDate}',issuecomment = '${row.Comment}'
    where IssueId = '${row.IssueID}'`,
    // no target
        `update IssueLog set issuedate = 'now()' , issuesummary = '${row.Summary}', issuedescription =
    '${row.Description}', projectcode = '${row.Project}', modulecode = '${row.Module}', defecttype = 
    '${row.DefectType}', issuestatus = '${row.Status}', issueseverity = '${row.Severity}',
    issuepriority = '${row.Priority}' ,closeddate = '${row.ClosedDate}',issuecomment = '${row.Comment}'
    where IssueId = '${row.IssueID}'`,
    // no closed
        `update IssueLog set issuedate = 'now()' , issuesummary = '${row.Summary}', issuedescription =
    '${row.Description}', projectcode = '${row.Project}', modulecode = '${row.Module}', defecttype = 
    '${row.DefectType}', issuestatus = '${row.Status}', issueseverity = '${row.Severity}',
    issuepriority = '${row.Priority}', targetfixdate = '${row.TargetFixDate}', issuecomment = '${row.Comment}'
    where IssueId = '${row.IssueID}'`,
    // not both
        `update IssueLog set issuedate = 'now()' , issuesummary = '${row.Summary}', issuedescription =
    '${row.Description}', projectcode = '${row.Project}', modulecode = '${row.Module}', defecttype = 
    '${row.DefectType}', issuestatus = '${row.Status}', issueseverity = '${row.Severity}',
    issuepriority = '${row.Priority}',issuecomment = '${row.Comment}'
    where IssueId = '${row.IssueID}'`
    ]

    if (row.TargetFixDate && row.ClosedDate ) {
        sql = sql[0]
    } else if (row.TargetFixDate && !row.ClosedDate) {
        sql = sql[2]
    } else if (!row.TargetFixDate&& row.ClosedDate) {
        sql = sql[1]
    } else {
        sql = sql[3]
    }
    try {
        await db.query(sql)
    } catch (err) {
        console.error(`${err}`)
    }
}

async function editIssueProjectDate(db, row) {
    let sql = `update Project set LastedDate = 'now()' where projectCode = '${row.ProjectCode}'`
    try {
        await db.query(sql)
    } catch (err) {
        console.error(`${err}`)
    }
}

async function deleteIssue(db, id) {
    let sql = `delete from issuelog where ID = '${id}'`
    try {
        await db.query(sql)
    } catch (err) {
        console.error(`${err}`)
    }
}