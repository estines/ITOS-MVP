module.exports = {
    createProject,
    createProjectStaffEmail,
    createProjectModule,
    findProjects,
    findProjectStaffEmailByCode,
    findProjectStaffEmails,
    findProjectModule,
    findProjectModuleByCode,
    editProject,
    editProjectStaffEmail,
    editProjectModule,
    removeProject,
    removeProjectStaffEmail,
    removeProjectStaffEmailByProjectCode,
    removeProjectModule,
    removeProjectModuleByProjectCode
}

async function createProject(db, row) {
    let sql = `insert into Project
        (ProjectCode, ProjectName, ProjectStatus) 
        values ($1,$2,$3) 
        returning id`
    let { rows } = await db.query(sql, [row.ProjectCode, row.ProjectName, row.ProjectStatus])
    return rows[0].id
}

async function createProjectStaffEmail(db, row) {
    let sql = `insert into ProjectStaffEmail
        (ProjectCode, StaffEmail)
        values ($1,$2)
        returning id`
    let { rows } = await db.query(sql, [row.ProjectCode, row.StaffEmail])
    return rows[0].id
}

async function createProjectModule(db, row) {
    let sql = `insert into ProjectModule
        (ProjectCode, ModuleName)
        values ($1,$2)
        returning ModuleCode`
    let { rows } = await db.query(sql, [row.ProjectCode, row.ModuleName])
    return rows[0].modulecode
}

async function editProject(db, row) {
    let sql = `update project set projectname = '${row.ProjectName}', projectstatus = '${row.ProjectStatus}' where projectcode = '${row.ProjectCode}'`
    await db.query(sql)
}

async function editProjectStaffEmail(db, row) {
    let sql = `update projectstaffemail set StaffEmail= '${row.StaffEmail}' where Id = '${row.Id}'`
    await db.query(sql)
}

async function editProjectModule(db, row) {
    let sql = `update projectmodule set ModuleName = '${row.ModuleName}' where ModuleCode = '${row.ModuleCode}'`
    await db.query(sql)
}

async function findProjects(db) {
    let sql = `select *, to_char("lasteddate",'DD/MM/YYYY') as date from Project`
    let { rows } = await db.query(sql)
    return rows
}

async function findProjectStaffEmails(db) {
    let sql = `select * from ProjectStaffEmail`
    let { rows } = await db.query(sql)
    return rows
}

async function findProjectModule(db) {
    let sql = `select * from ProjectModule`
    let { rows } = await db.query(sql)
    return rows
}

async function findProjectStaffEmailByCode(db, code) {
    let sql = `select * from projectstaffemail where projectcode = '${code}'`
    let { rows } = await db.query(sql)
    return rows
}

async function findProjectModuleByCode(db, code) {
    let sql = `select * from projectmodule where projectcode = '${code}'`
    let { rows } = await db.query(sql)
    return rows
}

async function removeProject(db, ProjectCode) {
    let sql = `delete from project where projectcode = '${ProjectCode}'`
    await db.query(sql)
}

async function removeProjectStaffEmail(db, Id) {
    let sql = `delete from projectstaffemail where id='${Id}'`
    await db.query(sql)
}

async function removeProjectStaffEmailByProjectCode(db, ProjectCode) {
    let sql = `delete from projectstaffemail where projectcode = '${ProjectCode}'`
    await db.query(sql)
}

async function removeProjectModule(db, ModuleCode) {
    let sql = `delete from projectmodule where ModuleCode = '${ModuleCode}'`
    await db.query(sql)
}

async function removeProjectModuleByProjectCode(db, ProjectCode) {
    let sql = `delete from projectmodule where projectcode = '${ProjectCode}'`
    await db.query(sql)
}