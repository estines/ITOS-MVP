const Redis = require("ioredis");
const session = require('koa-session')
const redis = new Redis()
let store = []
const config = {
    key: 'sess',
    httpOnly: true,
    maxAge: 30*60*10e2,
    store: {
        async get(key, maxAge, { rolling }){
            let sid = JSON.parse(await redis.get(key))
            console.log(sid)
            return sid
        },
        async set(key, sess, maxAge, { rolling }){
            try {
                await redis.set(key,JSON.stringify(sess),'EX',parseInt(maxAge)/1000)
            }
            catch(e) { }
        },
        async destroy(key){
            await redis.set(key, null)
        }
    }
}
module.exports = app => {
    app.use(session(config, app))
}