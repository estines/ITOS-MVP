const { Pool } = require('pg');
let config = {
    connectionString: `postgres://gmmzfahpblgahb:a51ea6153ff091a9be6d1852f40f1bfbd4a518d079ed960b9d025205dda709b8@ec2-54-221-207-184.compute-1.amazonaws.com:5432/d9famaevnnd7pk`,
    ssl: true,
}


const pool = new Pool(config)

module.exports = {
    getConnection: async (ctx, next) => {
        ctx.pool = pool
        await next()
    }
}



// const sqlPool = new sql.ConnectionPool({
//     host,
//     user,
//     database,
//     pool
// })

// module.exports = {


//     getConnection: async (ctx, next) => {
//         ctx.pool = sqlPool
//         await next()
//     }
// }

////////////////////////////////////////////////////////////


//! -- SQL Query Example --
// try {
//     ctx.db = await ctx.pool.connect()
//     // let query = 
//     //     `insert into Project (ProjectCode, ProjectName, ProjectStatus)
//     //     values ( '1003','test2','Active')`
//     let query = `select Id from Project where id = 1`
//     let { recordset } = await ctx.db
//         .request()
//         .query(query)
//     let [result] = recordset
//     console.log(result.Id)
// } finally {
//     await ctx.db.close()
// }