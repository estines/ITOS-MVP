import React, { Component } from 'react'
import { Select, Form, Modal, Button, Col, Row } from 'antd';
import ProjectForm from './ProjectForm'
import ModuleForm from './ModuleForm'
import ModuleList from './ModuleList'
import StaffEmailForm from './StaffEmailForm'
import StaffEmailList from './StaffList'

const FormItem = Form.Item;
const Option = Select.Option;

class EntryProject extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Id: '',
            ProjectCode: '',
            ProjectName: '',
            ProjectStatus: '',
            ProjectStaffEmail: [],
            ProjectModule: [],
            StaffEmailSelected: null,
            ModuleSelected: null,
            LastedUpdate:'',
            onSelectedStaffEmail: null,
            onSelectedModule: null,
        }

        this.handleOk = this.props.handleOk
        this.handleCancel = this.props.handleCancel
        this.createProject = this.props.onCreate
        this.editProject = this.props.onEdit
        //----- Staff ----//
        this.onEditStaff = this.onEditStaff.bind(this)
        this.onUpdateStaffEmail = this.onUpdateStaffEmail.bind(this)
        this.onDeleteStaff = this.onDeleteStaff.bind(this)
        this.getStaffEmailSeleted = this.getStaffEmailSeleted.bind(this)
        //----- Module ----//
        this.onDeleteModule = this.onDeleteModule.bind(this)
        this.onEditModule = this.onEditModule.bind(this)
        this.getModuleSeleted = this.getModuleSeleted.bind(this)
        this.onUpdateModule = this.onUpdateModule.bind(this)

        this.onSubmitHandler = this.onSubmitHandler.bind(this)
        this.getProjectCode = this.getProjectCode.bind(this)
        this.getProjectName = this.getProjectName.bind(this)
        this.getProjectStatus = this.getProjectStatus.bind(this)
        this.getProjectStaffEmail = this.getProjectStaffEmail.bind(this)
        this.getProjectModule = this.getProjectModule.bind(this)
    }

    // Check Incoming Props and set props to state

    componentWillReceiveProps(nextProps) {
        if (nextProps.selected) {
            let { selected } = nextProps
            this.setState({
                Id: selected.Id,
                ProjectCode: selected.ProjectCode,
                ProjectName: selected.ProjectName,
                ProjectStatus: selected.ProjectStatus,
                ProjectStaffEmail: selected.ProjectStaffEmail,
                ProjectModule: selected.ProjectModule,
                LastedUpdate: selected.LastedUpdate,
            })
        } else {
            this.setState({
                Id: '',
                ProjectCode: '',
                ProjectName: '',
                ProjectStatus: 'Active',
                ProjectStaffEmail: [],
                ProjectModule: [],
                LastedUpdate: ''
            })
        }
    }


    onHandleOk(e) {
        if (this.state.ProjectCode && this.state.ProjectName && this.state.ProjectStaffEmail && this.state.ProjectModule && this.state.ProjectStatus) {
            !this.props.selected ? this.createProject(this.state) : this.editProject(this.state)
            this.setState({
                Id: '',
                ProjectCode: '',
                ProjectName: '',
                ProjectStatus: '',
                ProjectStaffEmail: [],
                ProjectModule: [],
                LastedUpdate: ''
            })
        }
        else {
            alert('Please fill every form')
        }
    }

    //---------------------------- Cancel Modal ----------------------------------//
    onHandleCancel(e) {
        this.setState({
            ProjectCode: '',
            ProjectName: '',
            ProjectStatus: '',
            ProjectStaffEmail: [],
            ProjectModule: [],
            LastedUpdate: ''
        })
        this.handleCancel(e)
    }

    //------------------- create New Project & Edit Project -----------------------//
    onSubmitHandler(e) {
        if (this.state.ProjectCode && this.state.ProjectName && this.state.ProjectStaffEmail && this.state.ProjectModule && this.state.ProjectStatus) {
            !this.props.selected ? this.createProject(this.state) : this.editProject(this.state)
            this.setState({
                Id: '',
                ProjectCode: '',
                ProjectName: '',
                ProjectStatus: '',
                ProjectStaffEmail: [],
                ProjectModule: [],
                LastedUpdate: ''
            })
        } else {
            alert('Please fill a form completely before submit')
        }
    }

    //-------------------------------- Get getProject Code in value onChange ----------------------------------//
    getProjectCode(input) {
        this.setState({
            ProjectCode: input
        })
    }

    //-------------------------------- Get getProject Name in value onChange ----------------------------------//
    getProjectName(input) {
        this.setState({
            ProjectName: input
        })
    }

    //-------------------------------- Get getProject Status in value onChange ----------------------------------//
    getProjectStatus(input) {
        this.setState({
            ProjectStatus: input
        })
    }

    //----------------------------- Get Staff Emil in value onChange-----------------------------//
    getStaffEmailSeleted(input) {
        this.setState({
            StaffEmailSelected: input.target.value
        })
    }
    //----------------------------- Get Module in value onChange-----------------------------//
    getModuleSeleted(input) {
        this.setState({
            ModuleSelected: input
        })
    }
    //----------------------------- Add Staff Emil -----------------------------//
    getProjectStaffEmail(input) {
            let StaffEmail = {
                Id: this.state.ProjectStaffEmail.length !== 0 ? this.state.ProjectStaffEmail[this.state.ProjectStaffEmail.length - 1].Id + 1 : 1,
                ProjectCode: this.state.ProjectCode,
                StaffEmail: input
            }
            this.setState({
                ProjectStaffEmail: [...this.state.ProjectStaffEmail, StaffEmail],
                StaffEmailSelected: null
            })

    }

    //---------------------------- Add Project Module -------------------------//
    getProjectModule(input) {
        let Module = {
            ModuleCode: this.state.ProjectModule.length !== 0 ? this.state.ProjectModule[this.state.ProjectModule.length - 1].ModuleCode + 1 : 1,
            ProjectCode: this.state.ProjectCode,
            ModuleName: input
        }
        this.setState({
            ProjectModule: [...this.state.ProjectModule, Module],
            ModuleSelected: null
        })
    }

    //------------------------------- On Click Edit Staff Email  -----------------------------------//
    onEditStaff(rows) {
        this.setState({
            onSelectedStaffEmail: rows,
            StaffEmailSelected: rows.StaffEmail
        })
    }
    //------------------------------- On Click Edit Module  -----------------------------------//
    onEditModule(rows) {
        this.setState({
            onSelectedModule: rows,
            ModuleSelected: rows.ModuleName,
        })
    }

    //------------------------------- Update Module  -----------------------------------//
    onUpdateModule(input, Id) {
        let newModule = [...this.state.ProjectModule.map(item => {
            if (item.ModuleCode === Id) {
                item.ModuleCode = Id
                item.ModuleName = input
                return item
            }
            return item
        })]
        this.setState({
            onSelectedModule: null,
            ModuleSelected: null,
            ProjectModule: [...newModule]
        })
    }
    //------------------------------- Update Staff Email  -----------------------------------//
    onUpdateStaffEmail(input, Id) {
        let newEmail = [...this.state.ProjectStaffEmail.map(item => {
            if (item.Id === Id) {
                item.Id = Id
                item.StaffEmail = input
                return item
            }
            return item
        })]
        this.setState({
            onSelectedStaffEmail: null,
            StaffEmailSelected: null,
            ProjectStaffEmail: [...newEmail]
        })
    }


    //------------------------------- Delete Staff Email  -----------------------------------//
    onDeleteStaff(id) {
        this.setState({
            ProjectStaffEmail: [...this.state.ProjectStaffEmail.filter(item => {
                return item.Id !== id
            })]
        })
    }
    //------------------------------- Delete Module  -----------------------------------//
    onDeleteModule(id) {
        this.setState({
            ProjectModule: [...this.state.ProjectModule.filter(item => {
                return item.ModuleCode !== id
            })]
        })
    }

    //------------------------------- Render -----------------------------------//
    render() {
        // const Save = <Button type="primary" onClick={this.onSubmitHandler}>Save</Button>
        // const Update = <Button type="primary" onClick={this.onSubmitHandler}>Update</Button>

        return (
            <Modal
                className="modal-new-project"
                title="Project Profile-Entry"
                visible={this.props.visible}
                onOk={this.onSubmitHandler}
                confirmLoading={this.props.checkLoading}
                okText={this.props.selected ? "Update" : "Save"}
                onCancel={this.handleCancel}
                style={{ top: 15 }}
                width={"60%"}
            >
            
                <ProjectForm getCode={this.getProjectCode} getName={this.getProjectName} selected={this.state} editChecker={this.props.editChecker} />
                <StaffEmailForm
                    getStaffEmail={this.getProjectStaffEmail}
                    updateStaffEmail={this.onUpdateStaffEmail}
                    value={this.state.StaffEmailSelected}
                    getValue={this.getStaffEmailSeleted}
                    selected={this.state.onSelectedStaffEmail}
                    StaffEmails={this.state.ProjectStaffEmail}
                />
                <StaffEmailList
                    StaffEmails={this.state.ProjectStaffEmail}
                    onDelete={this.onDeleteStaff}
                    onEdit={this.onEditStaff}
                />

                <ModuleForm
                    updateModule={this.onUpdateModule}
                    getModule={this.getProjectModule}
                    value={this.state.ModuleSelected}
                    getValue={this.getModuleSeleted}
                    selected={this.state.onSelectedModule}
                    Modules={this.state.ProjectModule}
                />
                <ModuleList Modules={this.state.ProjectModule}
                    onDelete={this.onDeleteModule}
                    onEdit={this.onEditModule}
                />
                <Form>
                <Row>
                    <Col span={12}>
                    <FormItem
                        label="Status"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 14 }}
                    >
                        <Select
                            placeholder="Select Status"
                            value={this.state.ProjectStatus}
                            onChange={this.getProjectStatus}
                        >
                            <Option value="Active">Active</Option>
                            <Option value="Inactive">Inactive</Option>
                        </Select>
                    </FormItem>
                    </Col>
                    </Row>
                </Form>
                {/* <Form>
                    {this.props.selected ? Update : Save}
                    <Button type="primary">
                        Cancel
                    </Button>
                </Form> */}
            </Modal>
        )
    }
}
export default EntryProject