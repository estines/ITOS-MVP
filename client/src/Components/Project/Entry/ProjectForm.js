import React, { Component } from 'react'
import { Form, Input, Col, Row } from 'antd'
const FormItem = Form.Item;

class ProjrctForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ProjectCode: {},
            ProjectName: {},
        }
        this.onValidatingProjectCode = this.onValidatingProjectCode.bind(this)
        this.onValidatingProjectName = this.onValidatingProjectName.bind(this)
    }

    //------ Validate Cade-------
    onValidatingProjectCode(input) {
        if (input.target.value.length > 30) {
            this.setState({
                ProjectCode: {
                    validateStatus: 'error',
                    msg: 'Length must be less than 30 char'
                }
            })
        } else if (input.target.value.length === 0) {
            this.setState({
                ProjectCode: {
                    value: '',
                    validateStatus: 'warning',
                    msg: 'Please input Project Code'
                }
            })
            this.props.getCode(input.target.value)
        } else {
            this.setState({
                ProjectCode: {
                    value: input.target.value,
                    validateStatus : 'success',
                    msg : ''
                }
            })
            this.props.getCode(input.target.value)
        }
    }

    //------ Validate Name -------
    onValidatingProjectName(input) {
        if (input.target.value.length > 300) {
            this.setState({
                ProjectName: {
                    validateStatus: 'error',
                    msg: 'Length must be less than 300 char'
                }
            })
        } else if (input.target.value.length === 0) {
            this.setState({
                ProjectName: {
                    value: '',
                    validateStatus: 'warning',
                    msg: 'Please input Project Name'
                }
            })
            this.props.getName(input.target.value)
        } else {
            this.setState({
                ProjectName: {
                    value: input.target.value,
                    validateStatus: 'success',
                    msg: ''
                }
            })
            this.props.getName(input.target.value)
        }
    }


    render() {
        return (
            <Form>
                <Row>
                <Col span={12}>
                <FormItem
                    label="Project Code"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 14 }}
                    validateStatus={this.state.ProjectCode.validateStatus}
                    help={this.state.ProjectCode.msg}
                    hasFeedback
                >
                    <Input placeholder="Input Project Code" disabled={this.props.editChecker} value={this.props.selected ? this.props.selected.ProjectCode : this.state.ProjectCode.value} onChange={this.onValidatingProjectCode} />
                </FormItem>
                </Col>
                <Col span={12}>
                <FormItem
                    label="Project Name"
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 14 }}
                    validateStatus={this.state.ProjectName.validateStatus}
                    help={this.state.ProjectName.msg}
                    hasFeedback
                >
                    <Input placeholder="Input Project Name" value={this.props.selected ? this.props.selected.ProjectName : this.state.ProjectName.value} onChange={this.onValidatingProjectName} />
                </FormItem>
                </Col>
                </Row>
            </Form>
        )
    }
}

export default ProjrctForm