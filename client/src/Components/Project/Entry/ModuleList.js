import React, { Component } from "react"
import { Card, Table, Icon, Modal } from "antd"

export default class Search extends Component {
  
  onEditHandler(item, callback) {
    callback(item)
  }
  onDeleteHandler(item, callback) {
    Modal.confirm({
      title: 'Are you sure delete this Module?',
      content: `Module '${item.ModuleName}' will be removed.`,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        callback(item.ModuleCode)
      },
      onCancel() {
      }
    })
  }

  render() {
    const columns = [
      { title: 'Edit', dataIndex: '', key: 'x', render: item => <a onClick={() => this.onEditHandler(item, this.props.onEdit)}><Icon type="edit" /></a> },
      { title: 'Delete', dataIndex: '', key: 'rm', render: item => <a onClick={() => this.onDeleteHandler(item, this.props.onDelete)}><Icon type="delete" /></a> },
      { title: "Module", dataIndex: "ModuleName", key: "Module" }
    ]

    return (
      <Card className="list">
        <Table
          columns={columns}
          dataSource={this.props.Modules}
          rowKey={item => item.ModuleCode}
        />
      </Card>
    )
  }
}
