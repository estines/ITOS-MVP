import React, { Component } from 'react'
import { Form, Input, Button, Row, Col, Modal } from 'antd'
const FormItem = Form.Item;
let email

class StaffEmailForm extends Component {
    onAddHandler(callback) {        
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.input.value) && email.input.value.length <= 100) {
            let filterData = this.props.StaffEmails.filter(item => item.ProjectStaffEmail === email.input.value)
            console.log(filterData)
            if (filterData.length !== 0) {
                // alert('Duplicate Module')
                Modal.error({
                    title: 'This is an error message',
                    content: 'Duplicate Staff Email...'+filterData,
                  })

            } else {
                if (email.input.value) {
                    callback(email.input.value)
                    email.input.value = ''
                }
            }
        }
    }
    

    onUpdateHandler(callback) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.input.value) && email.input.value.length <= 100) {
            if (email.input.value) {
                callback(email.input.value, this.props.selected.Id)
                email.input.value = ''
            }
        }
    }
    onCancelHandler(callback) {
        callback(null, null)
        email.input.value = ''
    }

    onEnterHandler(event, create, update) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.input.value) && email.input.value.length <= 100) {
            this.props.selected ? update(email.input.value, this.props.selected.Id) : this.onAddHandler(create)
            email.input.value = ''
        }
    }


    render() {
        const { getFieldDecorator } = this.props.form
        const Add = <Button type="primary" onClick={() => this.onAddHandler(this.props.getStaffEmail)}>Add</Button>
        const Update = <div>
            <Button
                type="primary"
                onClick={() => this.onUpdateHandler(this.props.updateStaffEmail)}>
                Update
            </Button>
            <Button
                type="primary"
                onClick={() => this.onCancelHandler(this.props.updateStaffEmail)}>
                Cancel
            </Button>
        </div>

        return (
            <Form>
                <Row>
                    <Col span={12}>
                        <FormItem
                            label="Staff E-mail"
                            labelCol={{ span: 8 }}
                            wrapperCol={{ span: 14 }}
                            hasFeedback
                        >
                            {getFieldDecorator('email', {
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    min: 1, message: 'Please input your E-mail!',
                                }, {
                                    max: 100, message: 'Length must be less than 100 char',
                                }]
                            })(
                                <Input ref={node => email = node} placeholder="Input Project Staff E-mail "
                                    onPressEnter={e => this.onEnterHandler(e, this.props.getStaffEmail, this.props.updateStaffEmail)}
                                    type='email'
                                    value={this.props.value} onChange={this.props.getValue}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col span={12}>
                        <FormItem>
                            {this.props.selected ? Update : Add}
                        </FormItem>
                    </Col>
                </Row>
            </Form>
        )
    }
}



const WrappedRegistrationForm = Form.create()(StaffEmailForm)
export default WrappedRegistrationForm