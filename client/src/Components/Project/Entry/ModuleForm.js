import React, { Component } from 'react'
import { Form, Input, Button, Row, Col } from 'antd'
const FormItem = Form.Item;
let module

class ModuleForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ProjectModule: {},
        }
        this.onAddHandler = this.onAddHandler.bind(this)
        this.onUpdateHandler = this.onUpdateHandler.bind(this)
        this.onCancelHandler = this.onCancelHandler.bind(this)
        this.onEnterHandler = this.onEnterHandler.bind(this)
        this.onValidatingModule = this.onValidatingModule.bind(this)
    }

    onAddHandler(callback) {
        let filterData = this.props.Modules.filter(item => item.ModuleName === module.input.value)
        if (filterData.length !== 0) {
            alert('Duplicate Module')
        } else {
            if (module.input.value) {
                callback(module.input.value)
                module.input.value = ''
                this.setState({
                    ProjectModule: {},
                })
            }
        }
    }

    onUpdateHandler(callback) {
        let filterData = this.props.Modules.filter(item => item.ModuleName === module.input.value)
        if (filterData.length !== 0) {
            alert('Duplicate Module')
        } else {
            if (module.input.value.length > 0) {
                callback(module.input.value, this.props.selected.ModuleCode)
                module.input.value = ''
                this.setState({
                    ProjectModule: {},
                })
            }
        }
    }
    onCancelHandler(callback) {
        callback(null, null)
        module.input.value = ''
        this.setState({
            ProjectModule: {},
        })
    }

    onEnterHandler(event, callback) {
        if (module.input.value.length > 0) {
            this.props.selected ? this.props.updateModule(module.input.value, this.props.selected.ModuleCode) : this.onAddHandler(callback)
            module.input.value = ''
        }
    }
    //------ Validate Module -------
    onValidatingModule(input) {
        if (input.target.value.length > 300) {
            this.setState({
                ProjectModule: {
                    validateStatus: 'error',
                    msg: 'Length must be less than 300 char'
                }
            })
            return false
        } else if (input.target.value.length === 0) {
            this.setState({
                ProjectModule: {
                    value: '',
                    validateStatus: 'warning',
                    msg: 'Please input Project Name'
                }
            })
            this.props.getValue(input.target.value)
            return false
        } else {
            this.setState({
                ProjectModule: {
                    value: input.target.value,
                    validateStatus: 'success',
                    msg: ''
                }
            })
            this.props.getValue(input.target.value)
            return true
        }
    }


    render() {
        const Add = <Button type="primary" onClick={() => this.onAddHandler(this.props.getModule)}>Add</Button>
        const Update = <div>
            <Button type="primary" onClick={() => this.onUpdateHandler(this.props.updateModule)}>Update</Button>
            <Button type="primary" onClick={() => this.onCancelHandler(this.props.updateModule)}>Cancel</Button>
        </div>

        return (
            <Form>
                <Row>
                    <Col span={12}>
                <FormItem
                    label="Module"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 14 }}
                    validateStatus={this.state.ProjectModule.validateStatus}
                    help={this.state.ProjectModule.msg}
                    hasFeedback
                >
                    <Input ref={node => module = node}
                        placeholder="Input Module "
                        onPressEnter={e => this.onEnterHandler(e, this.props.getModule)}
                        value={this.props.value}
                        onChange={this.onValidatingModule}
                    />
                </FormItem>
                </Col>
                <Col span={12}>
                <FormItem>
                    {this.props.selected ? Update : Add}
                </FormItem>
                </Col>
                </Row>
            </Form>
        )
    }
}
export default ModuleForm