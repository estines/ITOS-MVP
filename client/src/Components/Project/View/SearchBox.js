import React, { Component } from "react";
import '../../../css/App.css'
import { Card, Input, Select, Form, Button, Col } from "antd";
const FormItem = Form.Item;
const { Option } = Select;
let nodeItems = { name: '', code: '' }

export default class SearchBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Keywords: { name: '', code: '', status: '' }
        }
        this.onCodeChangeHandler = this.onCodeChangeHandler.bind(this)
        this.onNameChangeHandler = this.onNameChangeHandler.bind(this)
        this.onStatusChangeHandler = this.onStatusChangeHandler.bind(this)
    }

    onNameChangeHandler(value) {
        this.setState({
            Keywords: { name: value.target.value, code: this.state.Keywords.code, status: this.state.Keywords.status }
        })
    }

    onCodeChangeHandler(value) {
        this.setState({
            Keywords: { name: this.state.Keywords.name, code: value.target.value, status: this.state.Keywords.status }
        })
    }

    onStatusChangeHandler(value) {
        this.setState({
            Keywords: { name: this.state.Keywords.name, code: this.state.Keywords.code, status: value }
        })
    }

    onPressEnterHandler(event, onSearchItem) {
        onSearchItem(this.state.Keywords)
    }

    onClickHandler(onSearchItem) {
        onSearchItem(this.state.Keywords)
    }

    onClear() {
        for (let input in nodeItems) {
            nodeItems[input].input.value = ''
        }
        this.setState({
            Keywords: { name: '', code: '', status: '' }
        })
        this.props.onSearch({ name: '', code: '', status: '' })
    }

    render() {
        let { onSearch } = this.props
        return (
            <Card>
                <Form>
                    <Col span={10} >
                    <FormItem
                        label="Project Code"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 12 }}
                    >
                        <Input
                            ref={node => nodeItems.code = node}
                            placeholder="Search for Project Code"
                            onChange={this.onCodeChangeHandler}
                            onPressEnter={e => this.onPressEnterHandler(e, onSearch)}
                        />
                    </FormItem>

                    </Col>
                    <Col span={14} >
                    <FormItem
                        label="Project Name"
                        labelCol={{ span: 6 }}
                        wrapperCol={{ span: 14 }}
                    >
                        <Input
                            ref={node => nodeItems.name = node}
                            placeholder="Search for Project Name"
                            onChange={this.onNameChangeHandler}
                            onPressEnter={e => this.onPressEnterHandler(e, onSearch)}
                        />
                    </FormItem>
                    </Col>
                    <Col span={10} >                    
                    <FormItem
                        label="Status"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 12 }}
                    >
                        <Select
                            placeholder="Select Status"
                            value={this.state.Keywords.status}
                            onChange={this.onStatusChangeHandler}
                        >
                                <Option value="">All</Option>
                                <Option value="Active">Active</Option>
                                <Option value="Inactive">Inactive</Option>
                            </Select>
                        </FormItem>
                    </Col>
                    <Col span={24} style={{textAlign:"Center"}}>
                    <FormItem
                        // wrapperCol={{ span: 12, offset: 5 }}
                    >
                        <Button
                            type="primary"
                            onClick={() => this.onClickHandler(onSearch)}
                        >
                            Search
                        </Button>
                        <Button
                            type="primary"
                            onClick={() => this.onClear()}
                        >
                            Cancel
                        </Button>
                    </FormItem>
                    </Col>
                </Form>
            </Card>
        );
    }
}