import React, { Component } from 'react'
import { Card, Modal, Table, Icon } from 'antd'


export default class ProjectList extends Component {
    onEditHandler(rows) {
        this.props.onEdit(rows)
    }


    onCheckHandler(data, searchData) {
        if (!searchData.length) {
            if (this.props.keywords.name !== '' || this.props.keywords.code !== '') {
                alert('No Data Found')
                return []
            }
            return data
        } else {
            return searchData
        }
    }

    onDeleteHandler(item, callback) {
        Modal.confirm({
            title: 'Do you want to delete?',
            content: `Project ${item.ProjectName} will be removed.`,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                callback(item.Id)
            },
            onCancel() {
            }
        })
    }

    render() {
        const columns = [
            { width: 80, fixed: 'left', title: 'Edit', dataIndex: '', key: 'x', render: item => <a onClick={() => this.onEditHandler(item)}><Icon type="edit" /></a> },
            { width: 80, fixed: 'left', title: 'Delete', dataIndex: '', key: 'rm', render: item => <a onClick={() => this.onDeleteHandler(item, this.props.onDelete)}><Icon type="delete" /></a> },
            { title: "Project Code", dataIndex: "ProjectCode", key: "projectcode" },
            { title: "Project Name", dataIndex: "ProjectName", key: "projectname" },
            { title: "Status", dataIndex: "ProjectStatus", key: "status" },
            { title: "List update date", dataIndex: "LastedUpdate", key: "listupdatedate" },
        ]

        return (
            <Card className="list" >
                <Table
                    loading={this.props.data.length > 0 || this.props.selectedData.length > 0 ? false : true}
                    dataSource={this.onCheckHandler(this.props.data, this.props.selectedData)}
                    columns={columns}
                    rowKey={record => record.Id}
                    pagination={{ pageSize: 20 }}
                    scroll={{ x: 900 }}
                />
            </Card >
        )
    }
}