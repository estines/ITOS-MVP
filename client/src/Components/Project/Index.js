import React, { Component } from 'react'
import { Button } from 'antd'
import SearchBox from './View/SearchBox'
import ProjectsList from './View/ProjectList'
import EntryProject from './Entry/Index'
import * as Api from '../../API/main'
let now = new Date()
let checkMonth = (now.getMonth() + 1).toString().length < 2 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1)
now = now.getDate() + '/' + checkMonth + '/' + now.getFullYear()

export default class Projects extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Keywords: { code: '', name: '', status: '' },
            selected: null,
            ProjectsList: [],
            ItemsList: [],
            editChecker: false,
            loading: false,
            visible: false
        }
        this.onCreateHandler = this.onCreateHandler.bind(this)
        this.onEditHandler = this.onEditHandler.bind(this)
        this.onClickEditHandler = this.onClickEditHandler.bind(this)
        this.onSearchHandler = this.onSearchHandler.bind(this)
        this.onDeleteHandler = this.onDeleteHandler.bind(this)
        this.showModal = this.showModal.bind(this)
        this.handleOk = this.handleOk.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
    }

    //{ Id: 1, ProjectCode: '1001', ProjectName: "Mock data S", ProjectStaffEmail: [{ Id: 1, StaffEmailCode: 1001, StaffEmail: "abcd@gmail.com" }], ProjectModule: [{ Id: 1, ModuleCode: 1001, Module: "module1" }], ProjectStatus: 'Active' },
    componentDidMount() {
        this.setState({
            ProjectsList: [
                ...this.props.ProjectLists
            ]
        })
    }

    async onCreateHandler(rows) {
        try {
            this.setState({
                loading: true
            })
            let insertId = await Api.postMethod('project', rows)
            rows.ProjectStaffEmail.map((item, index) => {
                item.Id = insertId.StaffEmails[index]
                return item
            })
            rows.ProjectModule.map((item, index) => {
                item.Id = insertId.Modules[index]
                return item
            })
            rows = { Id: insertId.Id, ProjectCode: rows.ProjectCode, ProjectName: rows.ProjectName, ProjectStaffEmail: rows.ProjectStaffEmail, ProjectModule: rows.ProjectModule, ProjectStatus: rows.ProjectStatus , LastedUpdate: now }
            this.setState({
                ProjectsList: [...this.state.ProjectsList, rows],
                loading: false,
                visible: false
            })
            alert('Create Complete!!')
        } catch (err) {
            alert('Create Incomplete!!')
            this.setState({
                loading: false,
                visible: false
            })
        }
    }

    onSearchHandler(keyword) {
        let newObj = {}
        for (let item in keyword) {
            if (item !== 'status') {
                newObj[item] = keyword[item]
                keyword[item] = new RegExp(keyword[item], 'i')
            }
            else {
                newObj[item] = keyword[item]
            }
        }
        this.setState({
            ItemsList: [...this.state.ProjectsList.filter(item => {
                if (keyword.status === '') {
                    return keyword.code.test(item.ProjectCode) && keyword.name.test(item.ProjectName)
                }
                return keyword.code.test(item.ProjectCode)
                    && keyword.name.test(item.ProjectName)
                    && keyword.status === item.ProjectStatus
            })],
            Keywords: newObj
        })
    }

    async onEditHandler(rows) {
        rows = { Id: rows.Id, ProjectCode: rows.ProjectCode, ProjectName: rows.ProjectName, ProjectStaffEmail: rows.ProjectStaffEmail, ProjectModule: rows.ProjectModule, ProjectStatus: rows.ProjectStatus }
        let insertId = await Api.patchMethod(`Project/edit`, rows)
        // rows.ProjectStaffEmail = insertId.ProjectStaffEmail
        // rows.ProjectModule = insertId.ProjectModule
        // console.log(insertId)
        let newList = this.state.ProjectsList.map(item => {
            if (item.Id === rows.Id) {
                return rows
            }
            return item
        })
        this.setState({
            ProjectsList: [...newList],
            editChecker: false,
            visible: false
        })
    }

    onClickEditHandler(rows) {
        this.setState({
            selected: rows,
            editChecker: true,
            visible: true
        })
    }

    async onDeleteHandler(id) {
        let [data] = this.state.ProjectsList.filter(item => { return item.Id === id })
        await Api.deleteMethod(`project/delete`, data)
        alert('Success!!')
        this.setState({
            ProjectsList: [...this.state.ProjectsList.filter(item => {
                return item.Id !== id
            })]
        })
    }

    //------------- Modal New Project --------------//
    showModal() {
        this.setState({
            visible: true
        })
    }

    handleOk(e) {
        this.setState({
            visible: false,
            selected: null,
            editChecker: false
        })
    }

    handleCancel(e) {
        this.setState({
            visible: false,
            selected: null,
            editChecker: false
        })
    }

    //--------------------- Render --------------------//
    render() {
        return (
            <div>
                <hr />
                <h2>Project Profile</h2>
                <div style={{ textAlign: "right" }}>
                    <Button type="primary" onClick={this.showModal} >New</Button>
                </div>
                <EntryProject
                    visible={this.state.visible}
                    handleOk={this.handleOk}
                    handleCancel={this.handleCancel}
                    onCreate={this.onCreateHandler}
                    onEdit={this.onEditHandler}
                    selected={this.state.selected}
                    editChecker={this.state.editChecker}
                    checkLoading={this.state.loading}
                />

                <SearchBox onSearch={this.onSearchHandler} style={{ marginBottom: 10}}/>
                <ProjectsList data={this.state.ProjectsList}
                    selectedData={this.state.ItemsList}
                    onDelete={this.onDeleteHandler}
                    onEdit={this.onClickEditHandler}
                    keywords={this.state.Keywords}
                />
            </div>
        )
    }
}
