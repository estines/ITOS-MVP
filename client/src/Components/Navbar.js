import React, { Component } from 'react'
import { Icon, Layout } from 'antd'
const { Header } = Layout

export default class Navbar extends Component {
    render() {
        return (
            <Header>
                <Icon
                    className="trigger"
                    type={this.collapsed ? "menu-unfold" : "menu-fold"}
                    onClick={this.props.onToggle}
                />
            </Header>
        )
    }
}