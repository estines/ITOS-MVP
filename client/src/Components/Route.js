import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Projects from './Project/Index'
import Issues from './Issue/Index'

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={() => <Projects ProjectLists={this.props.ProjectData} />} />
                <Route path="/Projects" component={() => <Projects ProjectLists={this.props.ProjectData} />} />
                <Route path="/Issues" component={() => <Issues ProjectLists={this.props.ProjectData} IssueList={this.props.IssueData}/>} />
            </Switch>
        )
    }
}