import React, { Component } from 'react'
import '../css/Main.css'
import { Layout } from 'antd'
import Navbar from './Navbar'
import Sidebar from './Sidebar'
import Breadcrumbs from './Breadcrumbs'
import Route from './Route'
import * as API from '../API/main'

const { Content } = Layout

export default class IssueTracking extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            projectData: [],
            issueData: [],
            collapsed: true
        }
        this.prepareProjectList = this.prepareProjectList.bind(this)
        this.prepareIssueList = this.prepareIssueList.bind(this)
        this.toggleHandler = this.toggleHandler.bind(this)
        this.setTitle = this.setTitle.bind(this)
        this.prepareProjectList()
        this.prepareIssueList()
    }

    componentDidMount() {
        this.prepareProjectList()
        this.prepareIssueList()
    }

    async prepareProjectList() {
        let result = await API.getMethod('project')
        this.setState({
            projectData: result ? [...result] : []
        })
    }

    async prepareIssueList() {
        let result = await API.getMethod('issue')
        this.setState({
            issueData: result ? [...result] : []
        })        
    }

    toggleHandler() {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    setTitle(title) {
        this.setState({
            title
        })
    }

    render() {
        return (
            <Layout>
                <Navbar
                    collapsed={this.state.collapsed}
                    onToggle={this.toggleHandler}
                />
                <Layout>
                    <Sidebar collapsed={this.state.collapsed} title={this.setTitle} />
                    <Content
                        style={{
                            minHeight: 280
                        }}
                    >
                        <Breadcrumbs title={this.state.title} />
                        <Route ProjectData={this.state.projectData} IssueData={this.state.issueData}/>
                    </Content>
                </Layout>
            </Layout>
        )
    }
}