import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Layout, Menu, Icon } from "antd"
const { Sider } = Layout

export default class Siderbar extends Component {
    render() {
        return (
            <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
                    <Menu.Item key="1">
                        <Link to="/Projects" onClick={() => this.props.title("Projects")}>
                            <Icon type="pie-chart" />
                            <span>Projects</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                        <Link to="/Issues" onClick={() => this.props.title("Issues")}>
                            <Icon type="code-o" />
                            <span>Issues</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
        )
    }
}