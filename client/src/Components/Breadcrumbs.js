import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Breadcrumb } from 'antd'

export default class Breadcrumbs extends Component {
    render() {
        const { title } = this.props
        return (
            <Breadcrumb style={{ margin: "16px 0" }}>
                <Breadcrumb.Item><Link to="/" >Home</Link></Breadcrumb.Item>
                <Breadcrumb.Item><Link to={title}>{title}</Link></Breadcrumb.Item>
            </Breadcrumb >
        )
    }
}