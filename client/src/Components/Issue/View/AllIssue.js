import React, { Component } from 'react'
import { Card, Table, Icon, Modal } from 'antd'

export default class AllIssue extends Component {
    constructor(props) {
        super(props)
        this.state = {
            IssueList: [],
            SearchList: []
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.IssueList) {
            this.setState({
                IssueList: [...nextProps.IssueList]
            })
        } else {
            this.setState({
                IssueList: []
            })
        }
        if (nextProps.SearchList) {
            this.setState({
                SearchList: [...nextProps.SearchList]
            })    
        } else {
            this.setState({
                SearchList: []
            })
        }
    }

    onClickDelete(item) {
        let { onDelete} = this.props
        Modal.confirm({
            title: 'Are you sure delete this Project?',
            content: `Project '${item.IssueID}' will be removed.`,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                onDelete(item.Id)
            },
            onCancel() {
            }
        })
    }

    onClickEdit(item) {
        this.props.onEdit(item)
    }

    onClickUpdate(item) {
        this.props.onUpdate(item)
    }

    checkList(Issue, Search) {
        if (!Search.length) {
            if (this.props.keyword.id || this.props.keyword.project || this.props.keyword.priority || this.props.keyword.severity || this.props.keyword.status || this.props.keyword.module) {
                // alert('not found')
                return []
            }
            return Issue
        }
        return Search
    }

    render() {
        const columns = [
            { width: 80, fixed: 'left', title: 'Edit', dataIndex: '', key: 'x', render: item => <a><Icon type="edit" onClick={()=> this.onClickEdit(item)}/></a> },
            { width: 80, fixed: 'left', title: 'Update Status', dataIndex: '', key: 'Update', render: item => <a><Icon type="check" onClick={()=> this.onClickUpdate(item)}/></a> },
            { width: 80, fixed: 'left', title: 'Delete', dataIndex: '', key: 'rm', render: item => <a><Icon type="delete" onClick={() => this.onClickDelete(item)} /></a> },
            { title: "Issue ID", dataIndex: "IssueID", key: "IssueID" },
            { title: "Summary", dataIndex: "Summary", key: "Summary" },
            { title: "Description", dataIndex: "Description", key: "Description" },
            { title: "Status", dataIndex: "Status", key: "Status" },
            { title: "Defect Type", dataIndex: "DefectType", key: "DefectType" },
            { title: "Severity", dataIndex: "Severity", key: "Severity" },
            { title: "Priority", dataIndex: "Priority", key: "Priority" },
            { title: "Target Fix Date", dataIndex: "TargetFixDate", key: "TargetFixDate" },
            { title: "Issue Date", dataIndex: "IssueDate", key: "IssueDate" },
            { title: "Closed Date", dataIndex: "ClosedDate", key: "ClosedDate" },
            { title: "Project", dataIndex: "Project", key: "Project" },
            { title: "Module", dataIndex: "Module", key: "Module" },
        ]

        return (
            <Card className="list" >
                <Table
                    dataSource={this.checkList(this.props.IssueList, this.props.SearchList)}
                    columns={columns}
                    pagination={{ pageSize: 20 }}
                    rowKey={item => item.Id}
                    scroll={{ x:1800 }} 
                    style={{ textAlign:"Center"}}
                />
            </Card >
        )
    }
}