import React, { Component } from "react"
import { Card, Col, Input, Select, Form, Button } from "antd"
const FormItem = Form.Item;
const { Option } = Select;
let keywords = { id: '', project: '', module: '', priority: '', severity: '', status: '' }

export default class SearchBox extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ProjectCode: '',
            Module: [],
        }
        this.onProjectChange = this.onProjectChange.bind(this)
    }

    onIssueIdChange(value) {
        keywords.id = value.target.value
    }

    onProjectChange(value) {
        keywords.project = value
        this.setState({
            ProjectCode: value,
            Module: [...this.props.ProjectList.filter(item => item.ProjectCode === value).map(item => {
                return item.ProjectModule
            })]
        })
    }

    onModuleChange(value) {
        keywords.module = value
    }

    onPriorityChange(value) {
        keywords.priority = value
    }

    onServerityChange(value) {
        keywords.severity = value
    }

    onStatusChange(value) {
        keywords.status = value
    }

    onPressEnterHandler(event, search) {
        search(keywords)
    }

    onClickHander(search) {
        search(keywords)
    }

    render() {
        let { onSearch, onClear, ProjectList } = this.props
        return (
            <Card>
                <Form>
                    <Col span={10}>
                        <FormItem
                            label="Issue ID"
                            labelCol={{ span: 8 }}
                            wrapperCol={{ span: 12 }}
                        >
                            <Input
                                placeholder="Search for Issue ID"
                                onChange={this.onIssueIdChange}
                                onPressEnter={e => this.onPressEnterHandler(e, onSearch)}
                            />
                        </FormItem>
                    </Col>
                    <Col span={14}>
                        <FormItem
                            label="Project"
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 10 }}
                        >
                            <Select
                                placeholder="Select Project"
                                onChange={this.onProjectChange}
                            >
                                {ProjectList.map(item => {
                                    return <Option key={item.Id} value={item.ProjectCode}>{item.ProjectCode}</Option>
                                })}
                            </Select>
                        </FormItem>
                    </Col>
                    <Col span={10}>
                        <FormItem
                            label="Module"
                            labelCol={{ span: 8 }}
                            wrapperCol={{ span: 12 }}
                        >
                            <Select
                                placeholder="Select Module"
                                onChange={this.onModuleChange}
                            >
                                {this.state.Module.length === 0 ? [] : this.state.Module[0].map(item => {
                                    return <Option key={item.ModuleCode} value={item.ModuleCode}>{item.ModuleName}</Option>
                                })}
                            </Select>
                        </FormItem>
                    </Col>
                    <Col span={14}>
                        <FormItem
                            label="Severity"
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 10 }}
                        >
                            <Select
                                placeholder="Select Severity"
                                onChange={this.onServerityChange}
                            >
                                <Option value="All">All</Option>
                                <Option value="Low">Low</Option>
                                <Option value="Medium">Medium</Option>
                                <Option value="High">High</Option>
                            </Select>
                        </FormItem>
                    </Col>
                    {/* <Col span={10}>
                        <FormItem
                        label="DatePicker"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 12 }}
                    >
                        {getFieldDecorator('date-picker', config)(
                            <DatePicker />
                        )}
                    </FormItem>
                    </Col> 
                    <Col span={14}>
                        <FormItem
                        label="TO"
                        labelCol={{ span: 6 }}
                         wrapperCol={{ span: 10 }}
                    >
                        {getFieldDecorator('date-picker', config)(
                            <DatePicker />
                        )}
                    </FormItem>
                    </Col> */}
                    <Col span={10} >
                        <FormItem
                            label="Status"
                            labelCol={{ span: 8 }}
                            wrapperCol={{ span: 12 }}
                        >
                            <Select
                                placeholder="Select Status"
                                onChange={this.onStatusChange}
                            >
                                <Option key='1' value='Open'>Open</Option>
                                <Option key='2' value='Fixed'>Fixed</Option>
                                <Option key='3' value='Return to Tester'>Return to Tester</Option>
                                <Option key='4' value='Drop'>Drop</Option>
                                <Option key='5' value='Issue'>Issue</Option>
                                <Option key='6' value='Reopen'>Reopen</Option>
                                <Option key='7' value='Closed'>Closed</Option>
                            </Select>
                        </FormItem>
                    </Col>
                    <Col span={14}>
                        <FormItem
                            label="Priority"
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 10 }}
                        >
                            <Select
                                placeholder="Select Severity"
                                onChange={this.onPriorityChange}
                            >
                                <Option value="All">All</Option>
                                <Option value="Low">Low</Option>
                                <Option value="Medium">Medium</Option>
                                <Option value="High">High</Option>
                            </Select>
                        </FormItem>
                    </Col>
                    <Col span={24} style={{ textAlign: "Center" }}>
                        <FormItem
                        // wrapperCol={{ span: 12, offset: 5 }}
                        >
                            <Button
                                type="primary"
                                onClick={() => this.onClickHander(onSearch)}
                            >
                                Search
                        </Button>
                            <Button type="primary"
                                onClick={() => onClear()}
                            >
                                Clear
                        </Button>
                        </FormItem>
                    </Col>
                </Form>
            </Card>
        )
    }
}