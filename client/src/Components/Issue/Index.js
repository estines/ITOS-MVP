import React, { Component } from 'react'
import { Button, message } from 'antd'
import AllIssue from './View/AllIssue'
import SearchBox from './View/SearchBox'
import EntryIssue from './Entry/Index'
import * as Api from '../../API/main'

export default class Issue extends Component {
    constructor(props) {
        super(props)
        this.state = {
            keywords: { id: '', project: '', module: '', priority: '', severity: '', status: '' },
            ProjectList: [],
            IssueList: [],
            SearchList: [],
            selected: null,
            openType: false,
            visible: false
        }
        this.showModal = this.showModal.bind(this)
        this.handleOk = this.handleOk.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        // Binding CRUD Method
        this.onCreateIssue = this.onCreateIssue.bind(this)
        this.onEditIssue = this.onEditIssue.bind(this)
        this.onClickEditIssue = this.onClickEditIssue.bind(this)
        this.onClickUpdateIssueStatus = this.onClickUpdateIssueStatus.bind(this)
        this.onDeleteIssue = this.onDeleteIssue.bind(this)
        // Binding Search Method
        this.onSearchIssue = this.onSearchIssue.bind(this)
        this.onClearSearchListHandler = this.onClearSearchListHandler.bind(this)
    }

    componentDidMount() {
        this.setState({
            ProjectList: [...this.props.ProjectLists],
            IssueList: [...this.props.IssueList]
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            ProjectList: [...nextProps.ProjectLists]
        })
    }

    // CRUD Method
    async onCreateIssue(rows) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        let newDate = [new Date(rows.TargetFixDate), new Date(rows.ClosedDate)]
        newDate[0] !== "Invalid Date" ? newDate[0] = [pad(newDate[0].getDate()), pad(newDate[0].getMonth() + 1), newDate[0].getFullYear()].join('/') : ""
        newDate[1] !== "Invalid Date" ? newDate[1] = [pad(newDate[1].getDate()), pad(newDate[1].getMonth() + 1), newDate[1].getFullYear()].join('/') : ""
        rows = {
            Id: this.state.IssueList.length + 1,
            IssueID: rows.Project + '-' + this.state.IssueList.filter(item => item.Project === rows.Project).length + 1,
            Summary: rows.Summary,
            Description: rows.Description,
            Status: rows.Status,
            DefectType: rows.DefectType,
            Severity: rows.Severity,
            Priority: rows.Priority,
            TargetFixDate: rows.TargetFixDate,
            IssueDate: rows.IssueDate,
            ClosedDate: rows.ClosedDate,
            Project: rows.Project,
            Module: rows.Module,
            Comment: rows.Comment,
            visible: true
        }
        let insertId = await Api.postMethod('issue', rows)
        rows.Id = insertId
        rows.TargetFixDate = newDate[0]
        rows.ClosedDate = newDate[1]
        this.setState({
            IssueList: [...this.state.IssueList, rows],
            openType: 'new',
            visible: false
        })
    }

    async onEditIssue(rows) {
        await Api.patchMethod('issue/edit', rows)
        let newList = this.state.IssueList.map(item => {
            if (item.Id === rows.Id) {
                return rows
            }
            return item
        })
        this.setState({
            IssueList: [...newList]
        })
    }

    onClickEditIssue(rows) {
        this.setState({
            selected: rows,
            visible: true,
            openType: 'edit'
        })
    }

    onClickUpdateIssueStatus(rows) {
        this.setState({
            selected: rows,
            visible: true,
            openType: 'update'
        })
    }

    async onDeleteIssue(id) {
        await Api.deleteMethod('issue/delete', { id })
        this.setState({
            IssueList: [...this.state.IssueList.filter(item => {
                return item.Id !== id
            })]
        })

    }

    //  Search Issue 
    onSearchIssue(keywords) {
        let newObj = {}
        for (let key in keywords) {
            if (key === 'status') {
                newObj[key] = keywords[key]
            } else {
                newObj[key] = keywords[key]
                keywords[key] = new RegExp(keywords[key], 'i')
            }
        }
        this.setState({
            SearchList: [...this.state.IssueList.filter(item => {
                if (keywords.status === '') {
                    return keywords.id.test(item.IssueID)
                        && keywords.project.test(item.Project)
                        && keywords.module.test(item.Module)
                        && keywords.priority.test(item.Priority)
                        && keywords.severity.test(item.Severity)
                }
                else {
                    return keywords.id.test(item.IssueID)
                        && keywords.project.test(item.Project)
                        && keywords.module.test(item.Module)
                        && keywords.priority.test(item.Priority)
                        && keywords.severity.test(item.Severity)
                        && keywords.status === item.Status
                }
            })],
            keywords: newObj
        })
    }

    onClearSearchListHandler() {
        message.config({
            top: '20%',
            duration: 2,
        })
        message.info('Search item was cleared')
        this.setState({
            SearchList: [],
            keywords: { id: '', project: '', module: '', priority: '', severity: '', status: '' }
        })
    }

    //------------- Modal New Issue --------------//
    showModal() {
        this.setState({
            visible: true
        })
    }
    handleOk(e) {
        this.setState({
            visible: false,
            selected: null,
        })
    }
    handleCancel(e) {
        this.setState({
            visible: false,
            selected: null,
        })
    }


    //--------------------- Render --------------------//
    render() {
        return (
            <div>
                <hr />
                <h2>Issue Tracking </h2>
                <div style={{ textAlign: "right" }}>
                    <Button type="primary" onClick={this.showModal} >New</Button>
                </div>
                <EntryIssue
                    visible={this.state.visible}
                    handleOk={this.handleOk}
                    handleCancel={this.handleCancel}
                    onCreate={this.onCreateIssue}
                    onEdit={this.onEditIssue}
                    ProjectList={this.state.ProjectList}
                    selected={this.state.selected}
                    openType={this.state.openType}
                    IssueList={this.state.IssueList}
                />
                <SearchBox onSearch={this.onSearchIssue} onClear={this.onClearSearchListHandler} ProjectList={this.state.ProjectList} />
                <AllIssue IssueList={this.state.IssueList} SearchList={this.state.SearchList} onDelete={this.onDeleteIssue} onEdit={this.onClickEditIssue} onUpdate={this.onClickUpdateIssueStatus} keyword={this.state.keywords} />
            </div>
        )
    }
}