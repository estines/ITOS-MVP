import React, { Component } from 'react'
import '../../../css/Main.css'
import { Col, Select, Form, Modal, Button, Input, Row } from 'antd';

const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;
let date = new Date()
date = date.getDate() + '/' + ((date.getMonth() + 1).toString().length === 1 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + '/' + date.getFullYear()

class EntryIssue extends Component {
    constructor(props) {
        super(props)
        this.state = {
            IssueList: [],
            Id: '',
            IssueID: '',
            Summary: '',
            Description: '',
            Status: '',
            DefectType: '',
            Severity: '',
            Priority: '',
            TargetFixDate: '',
            IssueDate: '',
            ClosedDate: '',
            Project: '',
            Module: '',
            Comment: '',
            ModuleList: [],
            StatusList: [],
            editStatus: false,
            openType: ''
        }
        this.handleOk = this.props.handleOk
        this.handleCancel = this.props.handleCancel
        this.onSubmitHandler = this.onSubmitHandler.bind(this)
        this.onIssueIDChange = this.onIssueIDChange.bind(this)
        this.onIssueDateChange = this.onIssueDateChange.bind(this)
        this.onDefectTypeChange = this.onDefectTypeChange.bind(this)
        this.onSummaryChange = this.onSummaryChange.bind(this)
        this.onCloseDateChange = this.onCloseDateChange.bind(this)
        this.onDescriptionChange = this.onDescriptionChange.bind(this)
        this.onModuleChange = this.onModuleChange.bind(this)
        this.onSeverityChange = this.onSeverityChange.bind(this)
        this.onProjectChange = this.onProjectChange.bind(this)
        this.onTargetFixDateChange = this.onTargetFixDateChange.bind(this)
        this.onStatusChange = this.onStatusChange.bind(this)
        this.onPriorityChange = this.onPriorityChange.bind(this)
        this.onCommentChange = this.onCommentChange.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selected) {
            let { selected, ProjectList, openType } = nextProps
            let now, check
            switch (selected.Status) {
                case `Open`:
                    now = [`Open`, `Fixed`, `Return to Tester`, `Drop`, `Issue`]
                    check = false
                    break
                case `Fixed`:
                    now = [`Fix`, `Repen`, `Closed`]
                    check = false
                    break
                case `Return to Tester`:
                    now = [`Reopen`, `Closed`]
                    check = false
                    break
                case `Drop`:
                    now = [`Drop`]
                    check = true
                    break
                case `Issue`:
                    now = [`Issue`, `Fixed`, `Return to Tester`, `Drop`]
                    check = false
                    break
                case `Reopen`:
                    now = [`Reopen`, `Fixed`, ` Return to Tester`, `Drop, Issue`]
                    check = false
                    break
                case `Closed`:
                    now = [`Closed`]
                    check = true
                    break
                default:
                    now = []
                    check = false
            }
            this.setState({
                Id: selected.Id,
                IssueID: selected.IssueID,
                Summary: selected.Summary,
                Description: selected.Description,
                Status: selected.Status,
                DefectType: selected.DefectType,
                Severity: selected.Severity,
                Priority: selected.Priority,
                TargetFixDate: selected.TargetFixDate,
                IssueDate: selected.IssueDate,
                ClosedDate: selected.ClosedDate,
                Project: selected.Project,
                Module: selected.Module,
                Comment: selected.Comment,
                ModuleList: [...ProjectList.filter(item => item.ProjectCode === selected.ProjectCode).map(item => {
                    return item.ProjectModule
                })],
                StatusList: [...now],
                editStatus: check,
                openType: openType
            })
        } else {
            this.setState({
                Id: '',
                IssueID: '',
                Summary: '',
                Description: '',
                Status: 'Open',
                DefectType: 'Coding',
                Severity: '',
                Priority: '',
                TargetFixDate: '',
                IssueDate: date,
                ClosedDate: '',
                Project: '',
                Module: '',
                Comment: '',
                ModuleList: [],
                StatusList: [],
                editStatus: false,
                openType: 'new'
            })
        }
    }

    onIssueIDChange(value) {
        this.setState({
            IssueID: value.target.value
        })
    }

    onSummaryChange(value) {
        this.setState({
            Summary: value.target.value
        })
    }

    onDescriptionChange(value) {
        this.setState({
            Description: value.target.value
        })

    }

    onStatusChange(value) {
        let now, check
        switch (value) {
            case `Open`:
                now = [`Open`, `Fixed`, `Return to Tester`, `Drop`, `Issue`]
                check = false
                break
            case `Fixed`:
                now = [`Fix`, `Repen`, `Closed`]
                check = false
                break
            case `Return to Tester`:
                now = [`Reopen`, `Closed`]
                check = false
                break
            case `Drop`:
                now = [`Drop`]
                check = true
                break
            case `Issue`:
                now = [`Issue`, `Fixed`, `Return to Tester`, `Drop`]
                check = false
                break
            case `Reopen`:
                now = [`Reopen`, `Fixed`, ` Return to Tester`, `Drop, Issue`]
                check = false
                break
            case `Closed`:
                now = [`Closed`]
                check = true
                break
            default:
                now = []
                check = false
        }
        this.setState({
            Status: value,
            StatusList: [...now],
            editStatus: check
        })
    }

    onDefectTypeChange(value) {
        this.setState({
            DefectType: value
        })
    }

    onSeverityChange(value) {
        this.setState({
            Severity: value
        })
    }

    onPriorityChange(value) {
        this.setState({
            Priority: value
        })
    }
    onTargetFixDateChange(value) {
        this.setState({
            TargetFixDate: value.target.value
        })
    }

    onIssueDateChange(value) {
        this.setState({
            IssueDate: value.target.value
        })
    }

    onCloseDateChange(date, dateString) {
        this.setState({
            ClosedDate: dateString
        })
    }

    onProjectChange(value) {
        this.setState({
            Project: value,
            ModuleList: [...this.props.ProjectList.filter(item => item.ProjectCode === value).map(item => {
                return item.ProjectModule
            })]
        })
    }

    onModuleChange(value) {
        this.setState({
            Module: value
        })
    }

    onCommentChange(value) {
        this.setState({
            Comment: value.target.value
        })
    }

    onSubmitHandler() {
        if (this.state.DefectType && this.state.Module && this.state.Priority && this.state.Project && this.state.Severity && this.state.Status && this.state.Summary) {
            this.props.selected ? this.props.onEdit(this.state) : this.props.onCreate(this.state)
            this.setState({
                Id: '',
                IssueID: '',
                Summary: '',
                Description: '',
                Status: '',
                DefectType: '',
                Severity: '',
                Priority: '',
                TargetFixDate: '',
                IssueDate: '',
                ClosedDate: '',
                Project: '',
                Module: ''
            })
            this.handleOk()
        } else {
            alert('Please fill a form completely before submit')
        }
    }

    render() {
        // const Save = <Button type="primary" onClick={this.onSubmitHandler}>Save</Button>
        // const Edit = <Button type="primary" onClick={this.onSubmitHandler}>Edit</Button>
        let { ProjectList } = this.props
        return (
            <Modal
                className="modal-new-project"
                title="Issue Tracking Entry"
                visible={this.props.visible}
                okText={this.props.selected ? 'Update' : 'Save'}
                confirmLoading={this.props.visible ? false : true}
                onOk={this.onSubmitHandler}
                onCancel={this.handleCancel}
                style={{ top: 15 }}
                width={"60%"}
            >
                <Form>
                    <Row>
                        <Col span={12}>
                            <FormItem
                                label="Issue ID"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Input placeholder="Issue ID" disabled={true} value={this.state.IssueID} onChange={this.onIssueIDChange} />
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Issue Date"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                {/* ---------- New DATE ---------------- */}
                                <Input placeholder="Issue Date" disabled={true} value={this.state.IssueDate === "" ? date : this.state.IssueDate} onChange={this.onIssueDateChange} />
                            </FormItem>
                        </Col>
                        <Col span={24}>
                            <FormItem
                                label="Summary"
                                labelCol={{ span: 3 }}
                                wrapperCol={{ span: 18 }}
                            >
                                <TextArea placeholder="Input Summary" disabled={this.state.openType === 'update' ? true : false} value={this.state.Summary} autosize={{ minRows: 2, maxRows: 6 }} onChange={this.onSummaryChange} />

                            </FormItem>
                        </Col>
                        <Col span={24}>
                            <FormItem
                                label="Description"
                                labelCol={{ span: 3 }}
                                wrapperCol={{ span: 18 }}
                            >
                                <TextArea placeholder="Input Description" disabled={this.state.openType === 'update' ? true : false} value={this.state.Description} autosize={{ minRows: 2, maxRows: 6 }} onChange={this.onDescriptionChange} />
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Project"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Select
                                    placeholder="Select Project"
                                    value={this.state.Project}
                                    onChange={this.onProjectChange}
                                    disabled={this.state.openType === 'update' ? true : false}
                                >
                                    {ProjectList.map(item => {
                                        return <Option key={item.Id} value={item.ProjectCode}>{item.ProjectCode}</Option>
                                    })}
                                </Select>
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Module"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Select
                                    placeholder="Select Module"
                                    value={this.state.Module}
                                    onChange={this.onModuleChange}
                                    disabled={this.state.openType === 'update' ? true : false}
                                >
                                    {this.state.ModuleList.length !== 0 ? this.state.ModuleList[0].map(item => {
                                        return <Option key={item.ModuleCode} value={item.ModuleCode}>{item.ModuleName}</Option>
                                    }) : []}
                                </Select>
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Defect Type"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Select
                                    placeholder="Select Defect Type"
                                    value={this.state.DefectType}
                                    onChange={this.onDefectTypeChange}
                                    disabled={this.state.openType === 'update' ? true : false}
                                >
                                    <Option value="Coding">Coding</Option>
                                    <Option value="Graphic">Graphic</Option>
                                    <Option value="Tester">Tester</Option>
                                    <Option value="Data Test">Data Test</Option>
                                    <Option value="Other">Other</Option>
                                </Select>
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Status"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Select
                                    defaultValue="Active"
                                    placeholder="Select Status"
                                    disabled={this.state.editStatus}
                                    value={this.state.Status}
                                    onChange={this.onStatusChange}
                                >
                                    {this.state.StatusList.length === 0 ?
                                        [
                                            <Option key='1' value='Open'>Open</Option>,
                                            <Option key='2' value='Fixed'>Fixed</Option>,
                                            <Option key='3' value='Return to Tester'>Return to Tester</Option>,
                                            <Option key='4' value='Drop'>Drop</Option>,
                                            <Option key='5' value='Issue'>Issue</Option>,
                                            <Option key='6' value='Reopen'>Reopen</Option>,
                                            <Option key='7' value='Closed'>Closed</Option>,
                                        ] : this.state.StatusList.map((item, idx) => {
                                            return <Option key={idx} value={item}>{item}</Option>
                                        })}
                                </Select>
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Severity"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Select
                                    placeholder="Select Severity"
                                    defaultValue="All"
                                    value={this.state.Severity}
                                    onChange={this.onSeverityChange}
                                    disabled={this.state.openType === 'update' ? true : false}
                                >
                                    <Option value="All">All</Option>
                                    <Option value="Low">Low</Option>
                                    <Option value="Medium">Medium</Option>
                                    <Option value="High">High</Option>
                                </Select>
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Priority"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Select
                                    placeholder="Select Priority"
                                    defaultValue="All"
                                    value={this.state.Priority}
                                    onChange={this.onPriorityChange}
                                    disabled={this.state.openType === 'update' ? true : false}
                                >
                                    <Option value="All">All</Option>
                                    <Option value="Low">Low</Option>
                                    <Option value="Medium">Medium</Option>
                                    <Option value="High">High</Option>
                                </Select>
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Target Fix Date"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >
                                <Input placeholder="Target Fix Date"
                                    value={this.state.TargetFixDate}
                                    onChange={this.onTargetFixDateChange}
                                    disabled={this.state.openType === 'update' ? true : false}
                                />
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                label="Closed Date"
                                labelCol={{ span: 6 }}
                                wrapperCol={{ span: 12 }}
                            >

                                <Input placeholder="Closed Date"
                                    disabled={true} //check if Edit flase
                                    value={this.state.Status === 'Closed' ? date : ''}
                                    onChange={this.onCloseDateChange}
                                />
                            </FormItem>
                        </Col>
                        <Col span={24}>
                            <FormItem
                                label="Comment"
                                labelCol={{ span: 3 }}
                                wrapperCol={{ span: 18 }}
                            >
                                <TextArea placeholder="Input Comment"
                                    autosize={{ minRows: 2, maxRows: 6 }}
                                    value={this.state.Comment}
                                    onChange={this.onCommentChange}
                                />

                            </FormItem>
                        </Col>
                    </Row>
                </Form>

            </Modal >
        )
    }
}
export default EntryIssue