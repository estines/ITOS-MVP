import React, { Component } from "react";
import { Card, Input, Select, Form, Button } from "antd";
import "../css/Main.css";
const FormItem = Form.Item;
const { Option } = Select;
let Keywords = { name: '', code: '' }

export default class Search extends Component {
  onSearchViaNameHandler(event, callback) {
    if (event.key === `Enter`) {
      callback(Keywords.name.input.value.trim())
    }
  }

  onSearchViaCodeHandler(event, callback) {
    if (event.key === `Enter`) {
      callback(Keywords.code.input.value.trim())
    }
  }

  onChangeHandler(value, callback) {
    callback(value)
  }

  render() {
    let { onSearch, projectCode, statusSearch } = this.props
    return (
      <Card>
        <Form>
          <FormItem
            label="Project Code"
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 5 }}
          >
            <Input
              ref={node => Keywords.code = node}
              placeholder="Search for Project Code"
              onKeyPress={(e) => this.onSearchViaCodeHandler(e, projectCode)} />
          </FormItem>
          <FormItem
            label="Project Name"
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 10 }}
          >
            <Input
              ref={node => Keywords.name = node}
              placeholder="Search for Project Name"
              onKeyPress={(e) => this.onSearchViaNameHandler(e, onSearch)}
            />
          </FormItem>

          <FormItem
            label="Status"
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 5 }}
          >
            <Select
              placeholder="Select Status"
              onChange={e => this.onChangeHandler(e, statusSearch)}
            >
              <Option value="all">All</Option>
              <Option value="Active">Active</Option>
              <Option value="Inactive">Inactive</Option>
            </Select>
          </FormItem>
          <FormItem
            wrapperCol={{ span: 12, offset: 5 }}
          >
            <Button type="primary">
              Save
            </Button>
            <Button type="primary">
              Cancel
            </Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}
