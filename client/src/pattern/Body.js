import React, { Component } from 'react'
import { Layout } from "antd"
import Sidebar from './Sidebar'
import Content from './Content'

class Body extends Component {
    render() {
        const { collapsed, data, searchData, onSearch, projectCode, onSearchStatus, onDelete, keywords } = this.props
        return (
            <Layout>
                <Sidebar collapsed={collapsed} />
                <Content datasource={data}
                    searchDatasource={searchData}
                    search={onSearch}
                    searchStatus={onSearchStatus}
                    deleteProject={onDelete}
                    searchCode={projectCode}
                    keywords={keywords}
                />
            </Layout>
        )
    }
}

export default Body;