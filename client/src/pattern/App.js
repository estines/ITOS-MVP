import React, { Component } from "react"
import "./css/App.css"
import Navbar from './components/Navbar'
import Body from './components/Body'
import Footer from "./components/Footer"
const url = `http://localhost:3000/project/`

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      collapsed: true,
      keyword: { code: '', name: '', status: '' },
      listAllProject: [],
      listAllIssue: [],
      listSearch: []
    }
    this.toggleHandler = this.toggleHandler.bind(this)
    this.deleteProject = this.deleteProject.bind(this)
    this.searchByProjectNameHandler = this.searchByProjectNameHandler.bind(this)
    this.searchByPojectCodeHandler = this.searchByPojectCodeHandler.bind(this)
    this.searchByProjectStatusHandler = this.searchByProjectStatusHandler.bind(this)
  }

  componentDidMount() {
    this.setState({
      listAllProject: [
        { Id: 1, ProjectCode: '1001', ProjectName: "Mock data S", ProjectStatus: 'Active' },
        { Id: 2, ProjectCode: '1002', ProjectName: "listAll", ProjectStatus: 'Inactive' },
        { Id: 3, ProjectCode: '1003', ProjectName: "MVC", ProjectStatus: 'Inactive' },
        { Id: 4, ProjectCode: '1004', ProjectName: "I wanna be Superman!!", ProjectStatus: 'Inactive' },
        { Id: 5, ProjectCode: '1005', ProjectName: "I need Money!", ProjectStatus: 'Inactive' },
        { Id: 6, ProjectCode: '1006', ProjectName: "Let me go", ProjectStatus: 'Active' },
        { Id: 7, ProjectCode: '1007', ProjectName: "Goku", ProjectStatus: 'Active' },
        { Id: 8, ProjectCode: '1008', ProjectName: "Master Data", ProjectStatus: 'Active' },
        { Id: 9, ProjectCode: '1009', ProjectName: "Pizza", ProjectStatus: 'Inactive' },
        { Id: 10, ProjectCode: '2000', ProjectName: "Api", ProjectStatus: 'Inactive' },
        { Id: 11, ProjectCode: '2001', ProjectName: "Router", ProjectStatus: 'Active' },
        { Id: 12, ProjectCode: '2002', ProjectName: "React", ProjectStatus: 'Active' },
        { Id: 13, ProjectCode: '2003', ProjectName: "Abcd", ProjectStatus: 'Active' }
      ]
    })
    // this.fetchGet('')
  }

  async fetch(route = '', opts = {}) {
    let data = await fetch(url + route, opts)
      .then(res => {
        return res.json()
      })
    return data
  }

  async fetchGet(route) {
    let result = await this.fetch(route)
    if (!route) {
      this.setState({
        listAllProject: [...result]
      })
    }
  }

  async createProject(row) {
    const result = await this.fetch('', {
      method: 'POST',
      Headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(row)
    })

    if (result) {
      let data = {
        Id: result,
        ProjectCode: row.ProjectCode,
        ProjectName: row.ProjectName,
        ProjectStatus: row.ProjectStatus
      }
      this.setState({
        listAllProject: [...this.state.listAllProject, data]
      })
    }
  }

  async createIssue(projectId, row) {
    const result = await this.fetch(`${projectId}/issue`, {
      method: 'POST',
      Headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(row)
    })

    if (result) {
      let data = {
        Id: result
      }
      this.setState({
        listAllIssue: [...this.state.listAllIssue, data]
      })
    }
  }

  // async fetchPatch(route, id, payload) {
  //   let data
  //   const result = await this.fetch(route, {
  //     method: 'PATCH',
  //     Headers: {
  //       Accept: 'application/json',
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify(payload)
  //   })

  //   if (result) {
  //     if (payload.ProjectCode) {
  //       // Project Entry
  //       data = {
  //         Id: id,
  //         ProjectCode: payload.ProjectCode,
  //         ProjectName: payload.ProjectName,
  //         ProjectStatus: payload.ProjectStatus
  //       }
  //       this.setState({
  //         listAllProject: [...(this.state.listAllProject.map(item => {
  //           if (item.Id === id) {
  //             return data
  //           }
  //           return item
  //         }))]
  //       })
  //     } else {
  //       // Issue Entry
  //       data = {
  //         Id: result
  //       }
  //       this.setState({
  //         listAllIssue: [...(this.state.listAllIssue.map(item => {
  //           if (item.Id === id) {
  //             return data
  //           }
  //           return item
  //         }))]
  //       })
  //     }

  //   }
  // }

  async deleteProject(projectId) {
    // let result = await this.fetch(projectId, {
    //   method: 'DELETE'
    // })
    // if (result) {
    //   this.setState({
    //     listAllProject: [...this.state.listAllProject.filter(item => {
    //       return item.Id !== projectId
    //     })]
    //   })
    // }
    if (projectId) {
      this.setState({
        listAllProject: [...this.state.listAllProject.filter(item => {
          return item.Id !== projectId
        })],
        listSearch: [
          ...this.state.listSearch.filter(item => {
            return item.Id !== projectId
          })
        ],
        keyword: { code: '', name: '', status: '' }
      })
    }
  }

  async deleteIssue(projectId, issueId) {
    let result = await this.fetch(`${projectId}/issue/${issueId}`, {
      method: 'DELETE'
    })
    if (result) {
      this.setState({
        listAllIssue: [...this.state.listAllIssue.filter(item => {
          return item.Id !== issueId
        })]
      })
    }
  }

  toggleHandler() {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  searchByProjectNameHandler(keyword) {
    let regExp = new RegExp(keyword, 'i')
    let newObj = {}
    for (let key in this.state.keyword) {
      if (key === 'name') {
        newObj[key] = keyword
      }
      else {
        newObj[key] = this.state.keyword[key]
      }
    }
    this.setState({
      listSearch: [
        ...this.state.listAllProject.filter(item => {
          return regExp.test(item.ProjectName)
        })
      ],
      keyword: newObj
    })
  }

  searchByPojectCodeHandler(keyword) {
    let regExp = new RegExp(keyword, 'i')
    let newObj = {}
    for (let key in this.state.keyword) {
      if (key === 'code') {
        newObj[key] = keyword
      }
      else {
        newObj[key] = this.state.keyword[key]
      }
    }
    this.setState({
      listSearch: [
        ...this.state.listAllProject.filter(item => {
          return regExp.test(item.ProjectCode)
        }
        )
      ],
      keyword: newObj
    })
  }

  searchByProjectStatusHandler(keyword) {
    let newObj = {}
    for (let key in this.state.keyword) {
      if (key === 'status') {
        newObj[key] = keyword
      }
      else {
        newObj[key] = this.state.keyword[key]
      }
    }
    this.setState({
      listSearch: [
        ...this.state.listAllProject.filter(item => {
          return item.ProjectStatus === keyword
        })
      ],
      keyword: newObj
    })
  }



  render() {
    return (
      <div>
        <Navbar
          collapsed={this.state.collapsed}
          onToggle={this.toggleHandler}
        />
        <Body
          collapsed={this.state.collapsed}
          data={this.state.listAllProject}
          searchData={this.state.listSearch}
          onSearch={this.searchByProjectNameHandler}
          projectCode={this.searchByPojectCodeHandler}
          onSearchStatus={this.searchByProjectStatusHandler}
          onDelete={this.deleteProject}
          keywords={this.state.keyword}
        />
        <Footer />
      </div>
    );
  }
}

export default App;