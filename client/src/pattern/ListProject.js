import React, { Component } from "react";
import { Card, Table, Modal } from "antd";
import "../css/Main.css";

export default class Search extends Component {
  onDeleteHandler(item,callback) {
    Modal.confirm({
      title: 'Are you sure delete this Project?',
      content: `Project '${item.ProjectName}' will be removed.`,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        callback(item.Id)
      },
      onCancel() {
      }
    })
  }

  onCheckHandler(data, searchData) {
    if (!searchData.length) {
      if(this.props.keywords.name !== '' || this.props.keywords.code !== '') {
        alert('not found')
      }
      return data
    } else {
      return searchData
    }
  }

  render() {
    const columns = [
      { title: 'Edit', dataIndex: '', key: 'x', render: item => <a>Edit</a> },
      { title: 'Delete', dataIndex: '', key: 'rm', render: item => <a onClick={() => this.onDeleteHandler(item,this.props.onDelete)}>Delete</a> },
      { title: "Project Code", dataIndex: "ProjectCode", key: "projectcode" },
      { title: "Project Name", dataIndex: "ProjectName", key: "projectname" },
      { title: "Status", dataIndex: "ProjectStatus", key: "status" },
      { title: "List update date", dataIndex: "listupdatedate", key: "listupdatedate" },
    ]
    return (
      <Card className="list">
        <Table
          dataSource={this.onCheckHandler(this.props.data, this.props.searchData)}
          columns={columns}
          rowKey={record => record.Id}
          pagination={{ pageSize: 20 }}
        />
      </Card>
    );
  }
}
