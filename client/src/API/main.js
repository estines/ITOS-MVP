const url = "http://localhost:3000/"

export async function request(path = '', req = {}) {
    try {
        let respond = await fetch(url + path, req).then(data => { return data.json() })
        if (respond) {
            return respond
        }
        return console.error('fetch failed, something wrong!!')
    } catch (err) {
        console.error(`Err: ${err}`)
    }
}

export async function getMethod(path = '') {
    let respond = await request(path)
    return respond

}

export async function postMethod(path, data) {
    let respond = await request(path, {
        method: 'POST',
        headers: {
            'Accept': 'apllication/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return respond

}

export async function patchMethod(path, data) {
    let respond = await request(path, {
        method: 'PATCH',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return respond

}

export async function deleteMethod(path, data) {
    let respond = await request(path, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    return respond
}